class SelectException(Exception):
    """Got Error to get data from select"""

class InsertException(Exception):
    """Got Error to insert data to db"""