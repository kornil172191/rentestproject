import datetime
from sqlalchemy.orm import as_declarative, Mapped, mapped_column, declared_attr
from typing import Optional

@as_declarative()
class AbstractModel:
    id: Mapped[int] = mapped_column(primary_key=True)
    @classmethod
    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()

class DataModel(AbstractModel):
    __tablename__ = 'xmls'
    type: Mapped[str]
    secID: Mapped[str]
    secType: Mapped[str]
    price: Mapped[int]
    date: Mapped[datetime.date]
    ImportedStatus: Mapped[Optional[str]]

    def __repr__(self) -> str:
        return (f'(type, {self.type}), (secID, {self.secID}),'
                f'(secType, {self.secType}), (price, {self.price}),'
                f'(date, {self.date}), (ImportedStatus, {self.ImportedStatus})')
    def __iter__(self) -> dict:
        result = self.__dict__
        del result['_sa_instance_state']
        yield from result.items()


