from src.XmlHandler import DataModel, ReqXMLGenerator, RepXMLGenerator, WorkDb
from loguru import logger

logger.add('main.logs', format='{time} {level} {message}', level='DEBUG', encoding='UTF8')


def main() -> None:
    logger.debug('Start main func')
    WorkDb.drop_table()
    WorkDb.create_table()
    WorkDb.insert_data(DataModel(type="type1", secID="label1", secType="Label", price=101, date="2/5/2024"))
    data = WorkDb.select_data(id=1)
    logger.info(f"Got data from DB by id: {data}")
    req_xml_obj = ReqXMLGenerator(type_value=data['type'], date_value=data['date'], label_value=data['secID'], price_value=data['price'])
    logger.info(f'Got values -> {req_xml_obj.__dict__} from {req_xml_obj.__class__.__name__}')
    req_xml_obj.generate_xml()
    rep_xml_object = RepXMLGenerator(type_value=data['type'], date_value=data['date'], label_value=data['secID'], price_value=data['price'], import_status='Imported')
    logger.info(f'Got values -> {rep_xml_object.__dict__} from {rep_xml_object.__class__.__name__}')
    rep_xml_object.generate_xml()
    logger.debug('End main func')

















if __name__ == "__main__":
    main()