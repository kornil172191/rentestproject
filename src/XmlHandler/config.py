import yaml

class Settings:
    def __init__(self):
        with open('config.yml', 'r') as stream:
            self.data = yaml.safe_load(stream)
    def get_db_full_address(self) -> str:
        return f"postgresql://{self.data['database']['username']}:{self.data['database']['password']}@{self.data['database']['host']}:{self.data['database']['port']}/{self.data['database']['db_name']}"

settings = Settings()

if __name__ == "__main__":
    # Пример использования
    print(settings.get_db_full_address())
