import datetime
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from datetime import datetime
@dataclass(frozen=True)
class ReqXMLGenerator:
    type_value: str
    date_value: datetime.date
    label_value: str
    price_value: int
    def generate_xml(self):
        root = ET.Element("root", attrib={"type": "update"})
        date = ET.SubElement(root, "date", attrib={"value": self.date_value.strftime('%Y-%m-%d')})
        type_elem = ET.SubElement(date, "type", attrib={"value": self.type_value})
        label = ET.SubElement(type_elem, "label", attrib={"value": self.label_value})
        price = ET.SubElement(label, "price")
        price.text = str(self.price_value)

        tree = ET.ElementTree(root)
        tree.write("templates/gen_request.xml")


if __name__ == '__main__':
    # Подставьте необходимые данные
    type_value = "type"
    date_value = "2024-02-04"
    label_value = "label3"
    price_value = 103

    xml_generator = ReqXMLGenerator(type_value, date_value, label_value, price_value)
    xml_generator.generate_xml()