import xml.etree.ElementTree as ET
from dataclasses import dataclass
from datetime import datetime
@dataclass(frozen=True)
class RepXMLGenerator:
    type_value: str
    date_value: datetime.date
    label_value: str
    price_value: int
    import_status: str

    def generate_xml(self):
        root = ET.Element("root", attrib={"type": "answer"})
        import_status = ET.SubElement(root, "importStatus")
        date = ET.SubElement(import_status, "date", attrib={"value": self.date_value.strftime('%Y-%m-%d')})
        type_elem = ET.SubElement(date, "type", attrib={"value": self.type_value})
        label = ET.SubElement(type_elem, "label", attrib={"value": self.label_value})
        price = ET.SubElement(label, "price")
        price.text = str(self.price_value)
        imported_status = ET.SubElement(label, "importedStatus", attrib={"value": self.import_status})

        tree = ET.ElementTree(root)
        tree.write("templates/gen_response.xml")

if __name__ == "__main__":
    # Подставьте необходимые данные
    type_value = "type"
    date_value = "2024-02-04"
    label_value = "label13"
    price_value = 103
    import_status = 'Imported'


    xml_generator = RepXMLGenerator(type_value, date_value, label_value, price_value, import_status)
    xml_generator.generate_xml()
