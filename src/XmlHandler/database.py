import datetime
import json
from sqlalchemy import create_engine, MetaData, select
from  sqlalchemy.orm import sessionmaker
from src.XmlHandler import AbstractModel, DataModel, SelectException, InsertException, settings
from collections import defaultdict
from typing import Callable
from loguru import logger

logger.add('db.logs', format='{time} {level} {message}', level='DEBUG', encoding='UTF8')


engine = create_engine(
    url=settings.get_db_full_address(),
    echo=True
)
metadata = MetaData()
session = sessionmaker(engine)


class WorkDb:
    """
    class to work with database
    """
    @staticmethod
    def create_table() -> None:
        logger.debug('Create all tables')
        AbstractModel.metadata.create_all(engine)

    @staticmethod
    def drop_table() -> None:
        logger.debug('Drop all tables')
        AbstractModel.metadata.drop_all(engine)
    @classmethod
    def select_data(cls, id) -> defaultdict:
        logger.debug('Start select data method')
        try:
            logger.debug('Connect to DB')
            with session() as conn:
                logger.debug(f"Got all data from table -> {DataModel.__tablename__}")
                query = conn.query(DataModel).filter_by(id=id).first()
                logger.info(f'Got result from table {DataModel.__tablename__}')
                return defaultdict(str, query)
        except Exception as error:
            logger.error(f"Got error from select data method -> {error}")
            raise SelectException
        finally:
            # cls.drop_table()
            ...
    @classmethod
    def insert_data(cls, data: Callable) -> None:
        try:
            logger.debug('Start insert data method')
            logger.debug('Connect to DB')
            with session() as conn:
                logger.debug(f'Insert data from parametrs: {data}')
                conn.add(data)
                logger.debug('Commit changes')
                conn.commit()
        except Exception as error:
            logger.error(f"Got error from insert data method -> {error}")
            raise InsertException
        finally:
            # cls.drop_table()
            ...
def main():
    logger.debug('Start main function')
    WorkDb.create_table()
    WorkDb.insert_data(DataModel(type="type1", secID="label1", secType="Label", price=101, date="2/5/2024"))
    logger.info(WorkDb.select_data(id=1))
    # WorkDb.drop_table()

if __name__ == '__main__':
    main()






